﻿using clubedepil.DOMAIN.DTOs;
using clubedepil.DOMAIN.DTOs.Return;

namespace clubedepil.DOMAIN.Interfaces
{
    public interface IProdutoRepository
    {
        Task<ProdutoReturnDTO> GetProduto(ProdutoDTO vm);
    }
}
