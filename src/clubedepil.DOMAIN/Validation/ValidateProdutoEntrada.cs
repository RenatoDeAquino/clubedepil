﻿using clubedepil.DOMAIN.DTOs;
using clubedepil.DOMAIN.DTOs.Return;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clubedepil.DOMAIN.Validation
{
    public class ValidateProdutoEntrada
    {
        public ApiReturn ValidateEntrada(ProdutoDTO req)
        {
            if (req is null)
            {
                return new ApiReturn
                {
                    tipoErro = 2,
                    codErro = 100,
                    msgErro = "ID não inserido",
                    OrigemErro = "APICluberDepil"
                };
            }

            return null;
        }
    }
}
