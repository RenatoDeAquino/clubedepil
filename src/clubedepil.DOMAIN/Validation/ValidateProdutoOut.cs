﻿using clubedepil.DOMAIN.DTOs.Return;

namespace clubedepil.DOMAIN.Validation
{
    public class ValidateProdutoOut
    {
        public ApiReturn ValidateOut(ProdutoReturnDTO req)
        {
            if (req is null)
                return new ApiReturn
                {
                    tipoErro = 2,
                    codErro = 101,
                    msgErro = "Produto não encontrado",
                    OrigemErro = "APIClubeDepil"
                };

            return null;
        }
    }
}
