﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clubedepil.DOMAIN.DTOs.Return
{
    public class ApiReturn
    {
        public int tipoErro { get; set; }
        public int codErro { get; set; }
        public string msgErro { get; set; }
        public string OrigemErro { get; set; }

    }
}
