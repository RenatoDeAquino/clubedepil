﻿namespace clubedepil.DOMAIN.DTOs.Return
{
    public class ProdutoReturnDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public Decimal Valor { get; set; }
    }
}
