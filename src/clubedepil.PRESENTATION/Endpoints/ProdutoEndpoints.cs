﻿using clubedepil.DOMAIN.DTOs;
using clubedepil.DOMAIN.DTOs.Return;
using clubedepil.DOMAIN.Interfaces;
using clubedepil.DOMAIN.Validation;
using FastEndpoints;
using Microsoft.AspNetCore.Authorization;
using System.Text.Json;

namespace clubedepil.PRESENTATION.Endpoints
{
    [HttpGet("api/v1/produto/{id}"), AllowAnonymous]
    public class ProdutoEndpoints : Endpoint<ProdutoDTO, ProdutoReturnDTO>
    {
        private readonly IProdutoRepository _repo;
        private readonly ValidateProdutoEntrada _validate;
        private readonly ValidateProdutoOut _validateOut;
        public ProdutoEndpoints(IProdutoRepository repo, ValidateProdutoEntrada validate, ValidateProdutoOut validateOut)
        {
            _repo = repo;
            _validate = validate;
            _validateOut = validateOut;
        }

        public override async Task HandleAsync(ProdutoDTO req, CancellationToken ct)
        {
            var val = _validate.ValidateEntrada(req);

            if (val is not null)
                ThrowError(JsonSerializer.Serialize(val));

            var consulta = await _repo.GetProduto(req);

            var valOut = _validateOut.ValidateOut(consulta);

            if (valOut is not null)
                ThrowError(JsonSerializer.Serialize(valOut));

            await SendOkAsync(consulta, ct);
        }
    }
}
