using clubedepil.DOMAIN.Interfaces;
using clubedepil.DOMAIN.Validation;
using clubedepil.INFRA.SQL.Connection;
using clubedepil.INFRA.SQL.Repository;
using FastEndpoints;
using FastEndpoints.Swagger;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddFastEndpoints();
builder.Services.AddCors();
builder.Services.AddSwaggerDoc(c =>
{
    c.Title = "Api Clube Depil";
    c.Version = "v1";
});

builder.Services.AddSingleton<ValidateProdutoEntrada>();
builder.Services.AddSingleton<ValidateProdutoOut>();
builder.Services.AddSingleton<IProdutoRepository, ProdutoRepository>();
builder.Services.AddSingleton<IDatabaseFactory, DatabaseFactory>();

var app = builder.Build();

app.UseCors(c =>
{
    c.AllowAnyHeader();
    c.AllowAnyMethod();
    c.AllowAnyOrigin();

});

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
app.UseFastEndpoints();
app.UseOpenApi();
app.UseSwaggerUi3(s => s.ConfigureDefaults());

app.UseHttpsRedirection();

app.Run();