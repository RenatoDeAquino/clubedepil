﻿namespace clubedepil.INFRA.SQL.Settings
{
    public class DatabaseSettings
    {
        private string _conString;

        public string DefaultConnection { get => _conString; }
        public DatabaseSettings(string conString)
        {
            _conString = conString;
        }

    }
}
