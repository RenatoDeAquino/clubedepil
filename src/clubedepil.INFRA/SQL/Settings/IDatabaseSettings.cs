﻿namespace clubedepil.INFRA.SQL.Settings
{
    public interface IDatabaseSettings
    {
        public string DefaultConnection { get; set; }
    }
}
