﻿using System.Data;

namespace clubedepil.INFRA.SQL.Connection
{
    public interface IDatabaseFactory
    {
        public IDbConnection GetDbConnection { get; }
    }

}
