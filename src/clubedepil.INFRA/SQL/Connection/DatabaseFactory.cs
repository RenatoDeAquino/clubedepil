﻿using clubedepil.INFRA.SQL.Settings;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace clubedepil.INFRA.SQL.Connection
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private DatabaseSettings dataSettings;
        protected string ConnectionString => dataSettings.DefaultConnection;
        public IDbConnection GetDbConnection => new SqlConnection(ConnectionString);

        public DatabaseFactory(IConfiguration configuration)
        {
            this.dataSettings = new DatabaseSettings(configuration.GetConnectionString("DefaultConnection"));
        }

    }
}
