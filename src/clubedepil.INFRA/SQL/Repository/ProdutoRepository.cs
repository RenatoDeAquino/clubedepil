﻿using clubedepil.DOMAIN.DTOs;
using clubedepil.DOMAIN.DTOs.Return;
using clubedepil.DOMAIN.Interfaces;
using clubedepil.INFRA.SQL.Connection;
using Dapper;

namespace clubedepil.INFRA.SQL.Repository
{
    public class ProdutoRepository : BaseRepository, IProdutoRepository
    {
        public ProdutoRepository(IDatabaseFactory connectionDB) : base(connectionDB)
        {
        }

        public async Task<ProdutoReturnDTO> GetProduto(ProdutoDTO vm)
        {
            using (var con = connectionDB.GetDbConnection)
            {
                var sql = @"select * from produto where Id = @Id";

                var query = (await con.QueryAsync<ProdutoReturnDTO>(sql, vm)).FirstOrDefault();

                return query;
            }
        }
    }
}
