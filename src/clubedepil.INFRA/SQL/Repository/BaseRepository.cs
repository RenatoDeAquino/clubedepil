﻿using clubedepil.INFRA.SQL.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clubedepil.INFRA.SQL.Repository
{
    public abstract class BaseRepository : IBaseRepository
    {
        public readonly IDatabaseFactory connectionDB;

        protected BaseRepository(IDatabaseFactory connectionDB)
        {
            this.connectionDB = connectionDB;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
